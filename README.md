# Phonofon
> The reference Client for [Tanzbox](https://gitlab.com/SirJson/Tanzbox)

## Why?
Because [Tanzbox]((https://gitlab.com/SirJson/Tanzbox)) is designed to be an audio controller / server and we also needed a client application so that someone could actually use our server.

We also call this the reference client because you are encouraged to write your own clients for Tanzbox. The language and UI toolkit choise is yours!

For example: This Project is built with Vue.js and TypeScript and is ment to be used in a local network as kind of a "Jukebox". But you could also write an desktop client for Tanzbox with the Server integrated so that you can use it as a local music player.

The protocol specification will be writen when things have more stablized on the server side. So your best *documentation* right now is the reference Client. (And of course the [Server](https://gitlab.com/SirJson/Tanzbox) itself)

## How do I get this to run?
A simple ``yarn install`` followed by a ``yarn serve`` should be enough to get you started! The client will then actively search for the server and retry if no service is available right now.

### Special thanks to:
* [Font Awesome](https://fontawesome.com) for letting me use the free icons they provide buttcher them up into an (temporary?) icon for my Project.
* [Flat UI Color](https://flatuicolors.com) ..because I never learned how colors work.
