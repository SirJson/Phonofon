/**
 * A Hashmap
 *
 * Yes you can only use strings for your keys
 * but that's just how JS rolls.
 * This interface just wraps the standard JS object.
 */
export interface HashMap {
  [key: string]: string;
}
