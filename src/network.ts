import {
  BasicCommand, ClientCommand, ServerCommand, ParseProtocol,
  CastProtocol, PlayerInfo, Service, SearchQuery, TrackRequest, PositionUpdate, Status
} from "./protocol";
import { HashMap } from "./hashmap";
import { store } from "./appdata";

const RECONNECT_TIMER_ADD = 15000;
const SERVER = "localhost";

let socket: WebSocket;
let reconnectTimer = 0;

export function isConnected() {
  return store.connected;
}

export function connect() {
  socket = new WebSocket(`ws://${SERVER}:8842`);
  socket.binaryType = "arraybuffer";
  socket.onclose = (evt) => onSocketClosed(evt);
  socket.onerror = (evt) => onSocketError(evt);
  socket.onmessage = (evt) => onSocketMessage(evt);
  socket.onopen = (evt) => onSocketOpen(evt);
}

export function disconnect() {
  socket.close(1, "disconnect");
}

export function onSocketOpen(event: Event) {
  reconnectTimer = 0;
  console.info("Connection successful!");
  store.connected = true;
  queryPlayerInfo();
}

export function onSocketClosed(event: CloseEvent) {
  store.connected = false;
  reconnectTimer += RECONNECT_TIMER_ADD;
  printCloseReason(event);
  console.warn(`Reconnecting in ${reconnectTimer / 1000}secs`);
  setTimeout(() => { connect(); }, reconnectTimer);
}

export function onSocketError(event: Event) {
  console.error("Socket Error: " + JSON.stringify(event));
}

export function printCloseReason(event: CloseEvent) {
  let status = "";
  const reason = event.reason !== "" ? `: ${event.reason}` : "";

  switch (event.code) {
    case 1000:
      status = "Normal Closure";
      break;
    case 1001:
      status = "Going Away";
      break;
    case 1002:
      status = "Protocol Error";
      break;
    case 1003:
      status = "Unsupported Data";
      break;
    case 1004:
      status = "???";
      break;
    case 1005:
      status = "No Status Recvd";
      break;
    case 1006:
      status = "Abnormal Closure";
      break;
    case 1007:
      status = "Invalid frame payload data";
      break;
    case 1008:
      status = "Policy Violation";
      break;
    case 1009:
      status = "Message too big";
      break;
    case 1010:
      status = "Missing Extension";
      break;
    case 1011:
      status = "Internal Error";
      break;
    case 1012:
      status = "Service Restart";
      break;
    case 1013:
      status = "Try Again Later";
      break;
    case 1014:
      status = "Bad Gateway";
      break;
    case 1015:
      status = "TLS Handshake";
      break;
    default:
      status = "Unknown";
      break;
  }
  const msg = `Connection closed! ${status}${reason}`;
  if (event.wasClean) {
    console.info(msg);
  } else {
    console.error(msg);
  }
}


export function onSocketMessage(event: MessageEvent) {
  const header = ParseProtocol(event.data);
  switch (header.command) {
    case ClientCommand.PlayerState:
      store.playerInfo = CastProtocol<PlayerInfo>(header);
      store.selectCurrentTrack();
      break;
    case ClientCommand.SearchTrackResult:
      store.searchResult = header.payload;
      store.searchRunning = false;
      break;
    case ClientCommand.QueueDirty:
      store.playerInfo.queue = header.payload;
      break;
    case ClientCommand.PlayTrack:
      store.playerInfo.current_track_id = header.payload;
      store.selectCurrentTrack();
      store.playerInfo.status = Status.Play;
      break;
    case ClientCommand.TrackStatusUpdate:
      store.playerInfo.status = header.payload as Status;
      break;
    case ClientCommand.TrackPositionUpdate:
      const update = CastProtocol<PositionUpdate>(header);
      store.playerInfo.time_elapsed = update.elapsed;
      store.playerInfo.time_total = update.length;
      break;
    default:
      console.error(`Unknown command received: ${header.command}`);
      console.debug(event.data);
      break;
  }
}

export function queryPlayerInfo() {
  console.debug("queryPlayerInfo");
  socket.send(JSON.stringify(new BasicCommand(ServerCommand.GetPlayerState)));
}

export function search(query: SearchQuery) {
  console.debug(`search: ${query.identifier} in ${query.service}`);
  socket.send(JSON.stringify(new BasicCommand(ServerCommand.SearchTrack, query)));
  store.searchRunning = true;
}

export function addTrackToQueue(service: Service, uri: string) {
  console.debug(`trackAdd: ${uri}`);
  const request = new TrackRequest(service, uri);
  socket.send(JSON.stringify(new BasicCommand(ServerCommand.AddTrack, request)));
}

export function playTrack(id: string) {
  console.debug(`trackPlay: ${id}`);
  socket.send(JSON.stringify(new BasicCommand(ServerCommand.QueuePlay, id)));
}

export function resumePlay() {
  console.debug("resumePlay");
  socket.send(JSON.stringify(new BasicCommand(ServerCommand.ResumePlay)));
}

export function pausePlay() {
  console.debug("pausePlay");
  socket.send(JSON.stringify(new BasicCommand(ServerCommand.PausePlay)));
}

export function skipForward() {
  console.debug("skipForward");
  socket.send(JSON.stringify(new BasicCommand(ServerCommand.SkipTrack)));
}

export function clearQueue() {
  console.debug("clearQueue");
  socket.send(JSON.stringify(new BasicCommand(ServerCommand.QueueClear)));
}
