export function stringPadLeft(str: string, pad: string, length: number) {
    return (new Array(length + 1).join(pad) + str).slice(-length);
}

export function formatSecToMinutes(time: number): string {
    const minutes = Math.floor(time / 60);
    const seconds = time - minutes * 60;
    const minuteStr = stringPadLeft(minutes.toString(), "0", 2);
    const secondsStr = stringPadLeft(seconds.toString(), "0", 2);
    return minuteStr + ":" + secondsStr;
}

export function formatMilliToMinutes(time: number): string {
    const evenSeconds = Math.floor(time / 1000); // Truncate miliseconds we don't want to represent
    return formatSecToMinutes(evenSeconds);
}

export function isMobileDevice() {
    return (typeof window.orientation !== "undefined") || (navigator.userAgent.indexOf("IEMobile") !== -1);
}
