import Vue from "vue";
import App from "./App.vue";
import router from "./router";
import * as network from "./network";
import { store } from "./appdata";

network.connect();

Vue.config.productionTip = false;

new Vue({
  router,
  render: (h) => h(App),
}).$mount("#app");
