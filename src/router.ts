import Vue from "vue";
import Router from "vue-router";
import Queue from "./views/Queue.vue";
import SearchMenu from "./views/SearchMenu.vue";
import SearchSoundcloud from "./views/SearchSoundcloud.vue";
import SearchSpotify from "./views/SearchSpotify.vue";
import SearchLocal from "./views/SearchLocal.vue";

Vue.use(Router);

export default new Router({
  routes: [
    {
      path: "/queue",
      name: "queue",
      component: Queue,
    },
    {
      path: "/search",
      name: "search",
      component: SearchMenu,
    },
    {
      path: "/search/soundcloud",
      name: "search-soundcloud",
      component: SearchSoundcloud,
    },
    {
      path: "/search/spotify",
      name: "search-spotify",
      component: SearchSpotify,
    },
    {
      path: "/search/local",
      name: "search-local",
      component: SearchLocal,
    },
  ],
});
