import { HashMap } from "./hashmap";

const Version = 0x02;

export enum Service {
    Local = "Local",
    Spotify = "Spotify",
    Youtube = "Youtube",
    SoundCloud = "SoundCloud",
}

export enum Status {
    Unknown = "Unknown",
    Play = "Play",
    Pause = "Pause",
    Stop = "Stop",
}

export interface Protocol {
    version: number;
    command: number;
    payload: any;
}

export interface Track {
    service: Service;
    metadata: HashMap;
    uuid: string;
}

export function nullTrack(): Track {
    const output = {} as Track;
    output.uuid = "";
    output.service = Service.Local;
    output.metadata = {} as HashMap;
    output.metadata["Artist"] = "";
    output.metadata["Title"] = "";
    return output;
}

export class PlayerInfo {
    public queue: Track[] = [];
    public current_track_index = 0;
    public current_track_id = "";
    public status = Status.Stop;
    public time_elapsed = 0;
    public time_total = 0;
    public time_left = 0;
}

export class SearchQuery {
    public identifier: string;
    public service: Service;
    public arguments?: HashMap;

    constructor(identifier: string, service: Service) {
        this.identifier = identifier;
        this.service = service;
    }
}

export class TrackRequest {
    public service: Service;
    public uri: string;

    constructor(service: Service, uri: string) {
        this.service = service;
        this.uri = uri;
    }
}

export class PositionUpdate {
    public elapsed = 0;
    public length = 0;

    constructor(elapsed: number, length: number) {
        this.elapsed = elapsed;
        this.length = length;
    }
}

export class BasicCommand implements Protocol {
    public version = 0x02;
    public command: number;
    public payload: any;

    constructor(cmd: number, payload: any = null) {
        this.command = cmd;
        this.payload = payload;
    }
}

export enum ClientCommand {
    Invalid = 0x00,
    Success = 0x01,
    Fail,
    TrackStatusUpdate,
    TrackPositionUpdate,
    PlayTrack,
    QueueDirty,
    Queue,
    SearchTrackResult,
    PlayerState
}

export enum ServerCommand {
    QueuePlay = 0x01,
    QueueRemove,
    ResumePlay,
    PausePlay,
    QueueClear,
    SkipTrack,
    GetQueue,
    GetPlayerState,
    AddTrack,
    SearchTrack
}

export function ParseProtocol(payload: string): Protocol {
    const protocol = JSON.parse(payload) as Protocol;
    return protocol.version === Version ? protocol : { command: ClientCommand.Invalid } as Protocol;
}

export function CastProtocol<T>(header: Protocol): T {
    return Object.assign(header, header.payload) as T;
}
