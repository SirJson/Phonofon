import { PlayerInfo, Track, nullTrack } from "./protocol";
import { HashMap } from "./hashmap";

const DebugMode = true;
const NullUuid = "00000000-0000-0000-0000-000000000000";

interface KeyValueStore {
    [key: string]: any;
}

export class AppData {
    public state: KeyValueStore = {};

    [key: string]: any;

    public connected: boolean = false;
    public playerInfo: PlayerInfo = new PlayerInfo();
    public searchResult: HashMap = {};
    public searchRunning: boolean = false;
    public currentTrack: Track = nullTrack();

    constructor() {
        console.group("Init data store...");
        for (const key of Object.keys(this)) {
            if (key === "state" || key === "debug") {
                continue;
            }
            const initValue = this[key];
            this.state[key] = initValue;
            delete this[key];
            Object.defineProperty(this, key, {
                get: (): any => this.state[key],
                set: (value: any) => {
                    if (DebugMode) {
                        console.group(`[MUTATION]: ${key}`);
                        console.dir(value);
                        console.groupEnd();
                    }
                    this.state[key] = value;
                },
                enumerable: true,
                configurable: true
            });
        }
        console.dir(this.state);
        console.groupEnd();
    }

    public selectCurrentTrack() {
        if (this.playerInfo.current_track_id === undefined || this.playerInfo.current_track_id === NullUuid) {
            return;
        }

        const result = store.searchTrack(this.playerInfo.current_track_id);
        if (result !== undefined) {
            this.currentTrack = result;
        } else {
            console.error("Failed to find Track", this.playerInfo.current_track_id);
        }
    }

    public searchTrack(uuid: string): Track | undefined {
        return this.playerInfo.queue.find((item) => item.uuid === uuid);
    }
}

export const store = new AppData();

interface Window {
    dumpStore(): void;
}
